let activeButton;
let activeText;
const ulMenu = document.querySelector('.tabs');
const liMenu = document.querySelectorAll('.tabs li');
const firstActive = document.querySelector('.tabs-title');
const liDescription = document.querySelectorAll('.tabs-content li');

liDescription.forEach((item) => {
  item.classList.add('text-description');
});
liDescription[0].classList.add('show');

setId(liMenu);
setId(liDescription);

ulMenu.onclick = function(event) {
  
   firstActive.classList.remove('active');
  liDescription[0].classList.remove('show');
  let target = event.target;
  
  if(target.tagName !='LI') return;

  higlight(target);
  const targetId = target.getAttribute('id');  
  showText(liDescription, targetId);

 
}

function higlight(li) {
  if(activeButton) {
    activeButton.classList.remove('active');
  }
  activeButton = li;
  activeButton.classList.add('active');

}
function setId(elem){
  for (let index = 0; index < elem.length; index++) {
    elem[index].id = `tab ${index}`;
  } 
}

function showText(elem, id) {
  elem.forEach(item => {
      if (item.id === id) {
          item.classList.add('show');
          activeText = item;
      } else {
          item.classList.remove('show');
      }
  })
}